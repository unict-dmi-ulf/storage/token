package it.unict.dmi.ulf.storage.token.repository;

import org.springframework.data.repository.CrudRepository;

import it.unict.dmi.ulf.storage.token.entity.Token;

public interface TokenRepository extends CrudRepository<Token, String> {
}
