package it.unict.dmi.ulf.storage.token.entity;

import java.net.URI;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
public class CourseAccess {
    @Id
    @GeneratedValue
    long value;

    @ManyToOne
    @JoinColumn(name="token")
    Token token;

    URI course;
}
