package it.unict.dmi.ulf.storage.token.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
public class Token {
    @Id
    String value;

    Date expirationDate;

    @OneToMany(cascade = CascadeType.ALL)
    Collection<CourseAccess> accessibleCourses;
}
