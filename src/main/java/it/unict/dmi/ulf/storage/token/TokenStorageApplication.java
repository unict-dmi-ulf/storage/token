package it.unict.dmi.ulf.storage.token;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TokenStorageApplication {
	public static void main(String[] args) {
		SpringApplication.run(TokenStorageApplication.class, args);
	}

}
