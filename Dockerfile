FROM gradle:6.8.3-jdk11 AS builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle clean build --no-daemon 

FROM openjdk:11 
WORKDIR /app
COPY --from=builder /home/gradle/src/build/libs/*.jar /app
ENV SERVER_PORT 8080
EXPOSE $SERVER_PORT
ENTRYPOINT ["java", "-jar", "ulf-storage-token.jar"]
